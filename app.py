import pandas as pd
import nltk
import numpy as np
import string
from unidecode import unidecode
import math
from nltk.corpus import stopwords

dict_size = lambda d: math.sqrt(sum(v**2 for v in d.values()))

def cosine_dist(d1, d2):
	dist = 0
	keys = list(set(d1.keys()) & set(d2.keys()))

	for key in keys:
		dist += d1[key]*d2[key]
	size = dict_size(d1)*dict_size(d2)
	if size == 0:
		cos_dist = 1
	else:
		cos_dist = 1-dist/size
	return cos_dist

def terke(text, n):
	vector = {}
	for i in range(0, len(text)-n+1):
		tup = text[i:i+n]
		if tup not in vector.keys():
			vector[tup] = 1
		else:
			vector[tup] += 1
	return vector

def clean_question(q):
	nopunc = "".join([c for c in q if c not in string.punctuation])
	nopunc = unidecode(nopunc).lower()
	return "".join([w for w in nopunc.split() if w not in stopwords.words('english')])

def nopunc_question(q):
	nopunc = "".join([c for c in q if c not in string.punctuation])
	return "".join(unidecode(nopunc).lower())

def create_bow(l, n):
	bow = {}
	for key in l.keys():
		bow[key] = terke(clean_question(l[key]), n)
	return bow

def predict_terka(q, a, text):
	best_el = None
	best_dist = 1
	for i in range(2, 10):
		txt = terke(text, i)
		bow = create_bow(q, i)
		for key in bow.keys():
			question = bow[key]
			dist = cosine_dist(txt, question)
			if best_dist > dist:
				best_dist = dist
				best_el = key
	return best_el, best_dist

def read_db(db):
	q = {}
	a = {}
	for index, row in db.iterrows():
		key = row["key"].split('_')
		if (key[1] == '?'):
			q[key[0]] = row["text"]
		else:
			a[key[0]] = row["text"]
	return q, a

def save_db(q, a, key, ans):
	i = 2
	while True:
		try:
			q[key+'.'+str(i)] = ans
			break
		except:
			i += 1
	
	t = {}
	for key in q.keys():
		t[key+"_?"] = q[key]
	q = t
	t = {}
	for key in a.keys():
		t[key+"_!"] = a[key]
	a = t

	dfq = pd.DataFrame(q.items(), columns=["key", "text"])
	dfa = pd.DataFrame(a.items(), columns=["key", "text"])
	df = pd.concat([dfq, dfa])
	df.sort_values("key", inplace=True)
	df.to_excel("database.xlsx", index=False)

if __name__ == "__main__":
	db = pd.read_excel("database.xlsx")
	q, a = read_db(db)

	while True:
		question = input("What would you like to know?: ")
		pred_key, dist = predict_terka(q, a, clean_question(question))
		if pred_key is None or dist > 0.5:
			#print("Most simmilar question:", q[pred_key])
			print("I have never seen this question before ... I will post it in the forums.")
			print("I will get to you with an answer shortly!")
			print("")
		else:
			ans = pred_key.split('.')[0]
			#print("Most simmilar question:", q[pred_key])
			print(a[ans])
			print("")
			""" s = input("Was this a good answer? ").lower()
			if s == "yes" or s == "y":
				save_db(q, a, ans, question)
			else:
				print("I will get to you with an answer shortly!")
			print("") """
